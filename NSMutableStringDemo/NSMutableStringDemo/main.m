//
//  main.m
//  NSMutableStringDemo
//
//  Created by darkerwei on 15/9/11.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableString *mutableString1 = [NSMutableString stringWithFormat:@"mutableString1"];
        NSLog(@"mutableString1:%@", mutableString1);
        
        NSMutableString *mutableString2 = [[NSMutableString alloc] initWithFormat:@"mutableString2"];
        NSLog(@"mutableString2:%@", mutableString2);
        
        NSMutableString *mutableString3 = [NSMutableString stringWithString:mutableString1];
        NSLog(@"mutableString3:%@", mutableString3);
        
        NSMutableString *mutableString4 = [[NSMutableString alloc] initWithString:mutableString2];
        NSLog(@"mutableString4:%@", mutableString4);
        
        NSMutableString *mutableString5 = [NSMutableString stringWithUTF8String:"mutableString5"];
        NSLog(@"mutableString5:%@", mutableString5);
        
        NSMutableString *mutableString6 = [[NSMutableString alloc] initWithUTF8String:"mutableString6"];
        NSLog(@"mutableString6:%@", mutableString6);
    }
    return 0;
}
