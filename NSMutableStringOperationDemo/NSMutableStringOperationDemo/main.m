//
//  main.m
//  NSMutableStringOperationDemo
//
//  Created by darkerwei on 15/9/11.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableString *mutableString = [[NSMutableString alloc] init];
        NSLog(@"mutableString:%@", mutableString);
        
        // 字符串拼接
        [mutableString appendFormat:@"String"];
        NSLog(@"mutableString:%@", mutableString);
        
        [mutableString appendString:@"String"];
        NSLog(@"mutableString:%@", mutableString);
        
        // 字符串插入
        static NSUInteger length = 12;
        
        if (mutableString.length >= length) {
            [mutableString insertString:@"String" atIndex:length];
            NSLog(@"mutableString:%@", mutableString);
        }
        
        // 字符串修改
        [mutableString setString:@"mutableString"];
        NSLog(@"mutableString:%@", mutableString);
        
        // 字符串子串范围
        NSRange range = [mutableString rangeOfString:@"mutable"];
        NSLog(@"%@", NSStringFromRange(range));
        
        // 字符串子串删除
        if (range.length != 0) {
            [mutableString deleteCharactersInRange:range];
            NSLog(@"mutableString:%@", mutableString);
        }
    }
    return 0;
}
