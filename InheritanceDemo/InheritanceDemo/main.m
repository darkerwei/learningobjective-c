//
//  main.m
//  InheritanceDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Student *student = [[Student alloc] init];
        student.name = @"wei";
        student.school = @"Harvard University";
        
        NSLog(@"student:%@", student);
        
        [student sleep];
        [student study];
    }
    return 0;
}
