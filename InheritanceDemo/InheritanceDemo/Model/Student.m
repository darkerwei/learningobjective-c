//
//  Student.m
//  InheritanceDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Student.h"

@implementation Student

- (void)study
{
    NSLog(@"study");
}

#pragma mark - description方法

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ name:%@, school:%@ }", self.name, self.school];
}

@end
