//
//  Person.h
//  InheritanceDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

/**
 *  姓名
 */
@property (nonatomic, copy) NSString *name;

/**
 *  睡觉
 */
- (void)sleep;

@end
