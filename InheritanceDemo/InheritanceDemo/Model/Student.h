//
//  Student.h
//  InheritanceDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"

@interface Student : Person

/**
 *  学校
 */
@property (nonatomic, copy) NSString *school;

/**
 *  学习
 */
- (void)study;

@end
