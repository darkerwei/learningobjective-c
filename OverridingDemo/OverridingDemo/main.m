//
//  main.m
//  OverridingDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *person = [[Person alloc] init];
        [person study];
        [person sleep];
        
        Student *student = [[Student alloc] init];
        [student study];
        [student sleep];
    }
    return 0;
}
