//
//  Person.m
//  OverridingDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"

@implementation Person

- (void)study
{
    NSLog(@"study");
}

- (void)sleep
{
    NSLog(@"sleep");
}

@end
