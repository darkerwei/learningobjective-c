//
//  Person.h
//  OverridingDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

/**
 *  学习
 */
- (void)study;
/**
 *  睡觉
 */
- (void)sleep;

@end
