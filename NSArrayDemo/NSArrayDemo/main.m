//
//  main.m
//  NSArrayDemo
//
//  Created by darkerwei on 15/9/11.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSArray *array1 = @[@"1", @"2"];
        NSLog(@"array1:%p-%@", array1, array1);
        
        NSArray *array2 = [NSArray arrayWithArray:array1];
        NSLog(@"array2:%p-%@", array2, array2);
        
        // 数组的遍历1
        for (NSString *string in array1) {
            NSLog(@"array1object:%p-%@", string, string);
        }
        
        // 数组的遍历2
        for (int i = 0; i < array2.count; ++i) {
            NSLog(@"array2object:%p-%@", array2[i], array2[i]);
        }
        
        // 数组的遍历3
        NSEnumerator *enumerator = [array1 objectEnumerator];
        NSString *string;
        
        while (string = [enumerator nextObject]) {
            NSLog(@"array1object:%p-%@", string, string);
        }
    }
    return 0;
}
