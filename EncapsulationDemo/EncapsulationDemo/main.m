//
//  main.m
//  EncapsulationDemo
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Computer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Computer *computer = [[Computer alloc] init];
        
        // 设置处理器
        [computer setCpu:7];
        // 设置屏幕尺寸
        [computer setInch:13];
        
        Person *person = [[Person alloc] init];
        
        // 设置姓名
        [person setName:@"wei"];
        // 设置年龄
        [person setAge:26];
        // 设置计算机
        [person setComputer:computer];
        
        NSLog(@"person:%@", person);
    }
    return 0;
}
