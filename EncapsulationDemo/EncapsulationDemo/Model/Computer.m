//
//  Computer.m
//  EncapsulationDemo
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Computer.h"

@interface Computer()

/**
 *  处理器
 */
@property (nonatomic, assign) NSUInteger cpu;
/**
 *  屏幕尺寸
 */
@property (nonatomic, assign) NSUInteger inch;

@end

@implementation Computer

#pragma mark - set方法

- (void)setCpu:(NSUInteger)cpu
{
    _cpu = cpu;
}

- (void)setInch:(NSUInteger)inch
{
    _inch = inch;
}

#pragma mark - description方法

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ cpu:%lu, inch:%lu }", self.cpu, self.inch];
}

@end
