//
//  Person.h
//  EncapsulationDemo
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Computer;

@interface Person : NSObject

/**
 *  姓名set方法
 *
 *  @param name 姓名
 */
- (void)setName:(NSString *)name;
/**
 *  年龄set方法
 *
 *  @param age 年龄
 */
- (void)setAge:(NSUInteger)age;
/**
 *  计算机set方法
 *
 *  @param computer 计算机
 */
- (void)setComputer:(Computer *)computer;

@end
