//
//  Computer.h
//  EncapsulationDemo
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Computer : NSObject

/**
 *  处理器set方法
 *
 *  @param cpu 处理器
 */
- (void)setCpu:(NSUInteger)cpu;
/**
 *  屏幕尺寸set方法
 *
 *  @param inch 屏幕尺寸
 */
- (void)setInch:(NSUInteger)inch;

@end
