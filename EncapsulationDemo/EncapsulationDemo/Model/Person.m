//
//  Person.m
//  EncapsulationDemo
//
//  Created by darkerwei on 15/9/9.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"
#import "Computer.h"

@interface Person()

/**
 *  姓名
 */
@property (nonatomic, copy) NSString *name;
/**
 *  年龄
 */
@property (nonatomic, assign) NSUInteger age;
/**
 *  计算机
 */
@property (nonatomic, strong) Computer *computer;

@end

@implementation Person

#pragma mark - set方法

- (void)setName:(NSString *)name
{
    _name = name;
}

- (void)setAge:(NSUInteger)age
{
    _age = age;
}

- (void)setComputer:(Computer *)computer
{
    _computer = computer;
}

#pragma mark - description方法

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ name:%@, age:%lu, computer:%@ }", self.name, self.age, self.computer];
}

@end
