//
//  Person+Extension.m
//  CategoryDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person+Extension.h"

@implementation Person (Extension)

- (void)eat
{
    NSLog(@"eat");
}

@end
