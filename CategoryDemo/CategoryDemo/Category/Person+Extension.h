//
//  Person+Extension.h
//  CategoryDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"

@interface Person (Extension)

/**
 *  吃饭
 */
- (void)eat;

@end
