//
//  main.m
//  CategoryDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Extension.h"
#import "Student.h"
#import "Person+Extension.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSString *string = [[NSString alloc] init];
        [string desc];
        
        Student *student = [[Student alloc] init];
        [student eat];
    }
    return 0;
}
