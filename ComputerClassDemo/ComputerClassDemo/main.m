//
//  main.m
//  ComputerClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Computer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *person = [[Person alloc] init];
        Computer *computer = [[Computer alloc] init];
        
        // 利用计算机学习
        [person studyWithComputer:computer];
    }
    return 0;
}
