//
//  Computer.m
//  ComputerClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Computer.h"

@implementation Computer

- (void)open
{
    NSLog(@"Computer open");
}

- (void)operation
{
    NSLog(@"Computer operation");
}

- (void)close
{
    NSLog(@"Computer close");
}

@end
