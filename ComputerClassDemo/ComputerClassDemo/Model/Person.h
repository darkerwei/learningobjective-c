//
//  Person.h
//  ComputerClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Computer;

@interface Person : NSObject

/**
 *  利用计算机学习
 *
 *  @param computer 计算机
 */
- (void)studyWithComputer:(Computer *)computer;

@end
