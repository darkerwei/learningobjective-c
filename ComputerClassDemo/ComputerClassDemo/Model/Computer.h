//
//  Computer.h
//  ComputerClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Computer : NSObject

/**
 *  开启
 */
- (void)open;
/**
 *  运算
 */
- (void)operation;
/**
 *  关闭
 */
- (void)close;

@end
