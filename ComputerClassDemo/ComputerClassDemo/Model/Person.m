//
//  Person.m
//  ComputerClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"
#import "Computer.h"

@implementation Person

- (void)studyWithComputer:(Computer *)computer
{
    // 开启
    [computer open];
    
    // 运算
    [computer operation];
    
    // 关闭
    [computer close];
}

@end
