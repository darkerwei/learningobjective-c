//
//  main.m
//  SelfDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *person = [Person person];
        person.name = @"wei";
        
        NSLog(@"person:%@", person);
    }
    return 0;
}
