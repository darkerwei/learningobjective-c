//
//  Person.h
//  SelfDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

/**
 *  姓名
 */
@property (nonatomic, copy) NSString *name;

/**
 *  创建Person对象
 *
 *  @return Person对象
 */
+ (instancetype)person;

@end
