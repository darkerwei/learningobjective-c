//
//  Person.m
//  SelfDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"

@implementation Person

+ (instancetype)person
{
    return [[self alloc] init];
}

#pragma mark - description方法

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ name:%@ }", self.name];
}

@end
