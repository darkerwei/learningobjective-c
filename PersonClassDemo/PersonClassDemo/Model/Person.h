//
//  Person.h
//  PersonClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

/**
 *  姓名
 */
@property (nonatomic, copy) NSString *name;
/**
 *  年龄
 */
@property (nonatomic, assign) NSUInteger age;

/**
 *  构造方法1
 *
 *  @param name 姓名
 *
 *  @return Person对象
 */
- (instancetype)initWithName:(NSString *)name;
/**
 *  构造方法2
 *
 *  @param age 年龄
 *
 *  @return Person对象
 */
- (instancetype)initWithAge:(NSUInteger)age;
/**
 *  构造方法3
 *
 *  @param name 姓名
 *  @param age  年龄
 *
 *  @return Person对象
 */
- (instancetype)initWithName:(NSString *)name age:(NSUInteger)age;

@end
