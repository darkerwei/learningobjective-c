//
//  Person.m
//  PersonClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import "Person.h"

@implementation Person

#pragma mark - 构造方法

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.name = name;
    }
    return self;
}

- (instancetype)initWithAge:(NSUInteger)age
{
    self = [super init];
    if (self) {
        self.age = age;
    }
    return self;
}

- (instancetype)initWithName:(NSString *)name age:(NSUInteger)age
{
    self = [super init];
    if (self) {
        self.name = name;
        self.age = age;
    }
    return self;
}

#pragma mark - description方法

- (NSString *)description
{
    return [NSString stringWithFormat:@"{ name:%@, age:%lu }", self.name, self.age];
}

@end
