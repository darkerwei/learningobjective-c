//
//  main.m
//  PersonClassDemo
//
//  Created by darkerwei on 15/9/8.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Person *person1 = [[Person alloc] init];
        NSLog(@"person1:%@", person1);
        
        // 构造方法1
        Person *person2 = [[Person alloc] initWithName:@"wei"];
        NSLog(@"person2:%@", person2);
        
        // 构造方法2
        Person *person3 = [[Person alloc] initWithAge:26];
        NSLog(@"person3:%@", person3);
        
        // 构造方法3
        Person *person4 = [[Person alloc] initWithName:@"wei" age:26];
        NSLog(@"person4:%@", person4);
    }
    return 0;
}
