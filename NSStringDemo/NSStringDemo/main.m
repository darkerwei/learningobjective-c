//
//  main.m
//  NSStringDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSString *string1 = [NSString stringWithFormat:@"string1"];
        NSLog(@"string1:%@", string1);
        
        NSString *string2 = [[NSString alloc] initWithFormat:@"string2"];
        NSLog(@"string2:%@", string2);
        
        NSString *string3 = [NSString stringWithString:string1];
        NSLog(@"string3:%@", string3);
        
        NSString *string4 = [[NSString alloc] initWithString:string2];
        NSLog(@"string4:%@", string4);
        
        NSString *string5 = [NSString stringWithUTF8String:"string5"];
        NSLog(@"string5:%@", string5);
        
        NSString *string6 = [[NSString alloc] initWithUTF8String:"string6"];
        NSLog(@"string6:%@", string6);
        
        NSString *string7 = @"string7";
        NSLog(@"string7:%@", string7);
    }
    return 0;
}
