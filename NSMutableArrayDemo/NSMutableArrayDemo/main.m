//
//  main.m
//  NSMutableArrayDemo
//
//  Created by darkerwei on 15/9/11.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSMutableArray *mutableArray1 = [[NSMutableArray alloc] init];
        NSLog(@"mutableArray1:%p-%@", mutableArray1, mutableArray1);
        
        // 元素添加
        [mutableArray1 addObject:@"1"];
        NSLog(@"mutableArray1:%p-%@", mutableArray1, mutableArray1);
        
        [mutableArray1 addObject:@"2"];
        NSLog(@"mutableArray1:%p-%@", mutableArray1, mutableArray1);
        
        [mutableArray1 addObject:@"3"];
        NSLog(@"mutableArray1:%p-%@", mutableArray1, mutableArray1);
        
        // 元素交换位置
        static NSUInteger length = 2;
        
        if (mutableArray1.count > length) {
            [mutableArray1 exchangeObjectAtIndex:0 withObjectAtIndex:length];
            NSLog(@"mutableArray1:%p-%@", mutableArray1, mutableArray1);
        }
        
        // 字符串转数组
        NSString *string1 = @"1-2-3-4-5";
        NSArray *array = [string1 componentsSeparatedByString:@"-"];
        NSLog(@"array:%p-%@", array, array);
        
        // 数组倒置
        NSMutableArray *mutableArray2 = [[NSMutableArray alloc] init];
        NSEnumerator *enumerator = [array reverseObjectEnumerator];
        NSString *string2;
        
        while (string2 = [enumerator nextObject]) {
            [mutableArray2 addObject:string2];
        }
        NSLog(@"mutableArray2:%p-%@", mutableArray2, mutableArray2);
    }
    return 0;
}
