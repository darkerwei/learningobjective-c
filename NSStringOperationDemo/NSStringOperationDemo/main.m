//
//  main.m
//  NSStringOperationDemo
//
//  Created by darkerwei on 15/9/10.
//  Copyright (c) 2015年 wei. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSString *string1 = @"12345";
        NSString *string2 = @"12345";
        NSString *string3 = @"123";
        
        // 字符串相同比较
        if ([string1 isEqualToString:string2]) {
            NSLog(@"string1 = string2");
        } else {
            NSLog(@"string1 != string2");
        }
        
        // 字符串大小比较
        NSComparisonResult result = [string2 compare:string3];
        
        if (result == NSOrderedAscending) {
            NSLog(@"string2 < string3");
        } else if (result == NSOrderedDescending) {
            NSLog(@"string2 > string3");
        } else if (result == NSOrderedSame) {
            NSLog(@"string2 = string3");
        }
        
        // 字符串截取
        static NSUInteger length = 5;
        
        if (string1.length >= length) {
            NSString *string4 = [string1 substringToIndex:length];
            NSLog(@"string4:%@", string4);
        }
        
        if (string1.length >= length) {
            NSString *string5 = [string1 substringFromIndex:length];
            NSLog(@"string5:%@", string5);
        }
        
        if (string1.length >= length) {
            NSString *string6 = [string1 substringWithRange:NSMakeRange(1, length - 1)];
            NSLog(@"string6:%@", string6);
        }
        
        // 字符串子串范围
        NSRange range = [string1 rangeOfString:string3];
        NSLog(@"%@", NSStringFromRange(range));
    }
    return 0;
}
